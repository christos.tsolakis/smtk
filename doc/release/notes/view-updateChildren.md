## PhraseModel

+ An error in the logic of `PhraseModel::updateChildren()` was fixed that
  could sometimes result in infinite loops trying to report reorders of an
  item's subphrases.
