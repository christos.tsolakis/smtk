## Changes to Resource
### Removed `smtk::resource::Set`
`smtk::resource::Set` has been removed from SMTK. It's original purpose was to
hold a collection of resources for export. This functionality has been
superceded by Project.
